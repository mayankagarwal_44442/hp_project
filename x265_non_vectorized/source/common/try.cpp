// OpenMP program to print Hello World 
// using C language 
  
// OpenMP header 
#include <omp.h> 
  
#include <stdio.h> 
#include <stdlib.h> 
  
int main(int argc, char* argv[]) 
{ 
    long long int n = 1000;


    #pragma omp parallel for
    for(long long int i = 0; i < n; ++i)
    {
        1 + 1;
        printf("%d\n", omp_get_thread_num());
    }
    #pragma end omp parallel	

    // // Beginning of parallel region 
    // #pragma omp parallel num_threads(3)
    // { 
  
    //     printf("Hello World... from thread = %d\n", 
    //            omp_get_thread_num()); 
    // } 
    // // Ending of parallel region 
}