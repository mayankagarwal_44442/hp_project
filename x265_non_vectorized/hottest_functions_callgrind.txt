Terms:
inclusive cost - cost including all function calls it makes
exclusive cost - cost of function itself
call count

Function name, inclusive cost, exclusive cost, call count
1) satd_8x4 : 8.05, 8.05, 76 088 220
2) sad_x3<8,8> : 3.78, 3.78, 5 770 093
3) sa8d_8x8 : 3.62, 3.62, 15 291 528
4) partialButterfly32 : 3.59, 3.59, 227 346
5) quant_c : 3.46, 3.46, 3 736 074
