#include <stdio.h>
#include <immintrin.h>
#include <stdint.h>
#include <chrono>
#include <iostream>

typedef uint8_t  pixel;

template<int lx, int ly>
void sad_x3(const pixel* pix1, const pixel* pix2, const pixel* pix3, const pixel* pix4,intptr_t frefstride, int32_t* res)
{
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    res[0] = 0;
    res[1] = 0;
    res[2] = 0;
    for (int y = 0; y < ly; y++)
    {
        for (int x = 0; x < lx; x++)
        {
            res[0] += abs(pix1[x] - pix2[x]);
            res[1] += abs(pix1[x] - pix3[x]);
            res[2] += abs(pix1[x] - pix4[x]);
        }

        pix1 += 64;
        pix2 += frefstride;
        pix3 += frefstride;
        pix4 += frefstride;
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() << "[ns]" << std::endl;
}

int main()
{
        // __m256i first = _mm256_set_epi16(10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25);
  //       __m256i res = _mm256_hadd_epi16(first, first);
  //       __m128i sum_high = _mm256_extracti128_si256(res, 1);  
  //       __m128i result = _mm_add_epi16(sum_high, _mm256_castsi256_si128(res));
  //       result = _mm_hadd_epi16(result, result); 
  //       result = _mm_hadd_epi16(result, result);
  //       printf("%d\n", ((short*)&result)[0]);
    pixel arr[64*64];
    int32_t res[3];
    sad_x3<64,64>(arr, arr, arr, arr, 64, res);

}