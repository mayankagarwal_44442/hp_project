import matplotlib.pyplot as plt
import numpy as np

x1 = [103662, 115680, 124901, 149954, 90709]
y1 = [1,2,3,4,5]
x2 = [130530, 153980, 151902, 152028, 132806]
y2 = [1,2,3,4,5]

plt.plot(y1,x1,'g', label = "With Vectorization")
plt.plot(y2,x2,'r', label = "Without Vectorization")
plt.xlabel("Trial Number ----------->")
plt.ylabel("Time in ns ----------->")
plt.legend(loc="upper left")
plt.show()
