#include <stdio.h>
#include <immintrin.h>
#include <stdint.h>
#include <chrono>
#include <iostream>

typedef uint8_t  pixel;
template<int lx, int ly>
void sad_x4(const pixel* pix1, const pixel* pix2, const pixel* pix3, const pixel* pix4, const pixel* pix5, intptr_t frefstride, int32_t* res)
{
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    res[0] = 0;
    res[1] = 0;
    res[2] = 0;
    res[3] = 0;


    __m256i pix1first;
    __m256i pix1second;
    __m256i pix1third;
    __m256i pix1fourth;
    __m256i pix2first;
    __m256i pix2second;
    __m256i pix2third;
    __m256i pix2fourth;
    __m256i pix3first;
    __m256i pix3second;
    __m256i pix3third;
    __m256i pix3fourth;
    __m256i pix4first;
    __m256i pix4second;
    __m256i pix4third;
    __m256i pix4fourth;
    __m256i pix5first;
    __m256i pix5second;
    __m256i pix5third;
    __m256i pix5fourth;

    __m256i res11;
    __m256i res12;
    __m256i res13;    
    __m256i res14;    
    __m256i res21;    
    __m256i res22;
    __m256i res23;    
    __m256i res24;       
    __m256i res31;    
    __m256i res32;
    __m256i res33;    
    __m256i res34;
    __m256i res41;    
    __m256i res42;
    __m256i res43;    
    __m256i res44;

    __m256i sum;
    __m128i sum_high;   
    __m128i result1;
    __m128i result2;

    for (int y = 0; y < ly; y++)
    {   
        if(lx == 8)
        {
            pix1first  = _mm256_set_epi16(pix1[0],pix1[1],pix1[2],pix1[3],pix1[4],pix1[5],pix1[6],pix1[7],
            0,0,0,0,0,0,0,0);
            pix1second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix1third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix1fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

            pix2first  = _mm256_set_epi16(pix2[0],pix2[1],pix2[2],pix2[3],pix2[4],pix2[5],pix2[6],pix2[7],
            0,0,0,0,0,0,0,0);
            pix2second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix2third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix2fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

            pix3first  = _mm256_set_epi16(pix3[0],pix3[1],pix3[2],pix3[3],pix3[4],pix3[5],pix3[6],pix3[7],
            0,0,0,0,0,0,0,0);
            pix3second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix3third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix3fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

            pix4first  = _mm256_set_epi16(pix4[0],pix4[1],pix4[2],pix4[3],pix4[4],pix4[5],pix4[6],pix4[7],
            0,0,0,0,0,0,0,0);
            pix4second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix4third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix4fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);


            pix5first  = _mm256_set_epi16(pix5[0],pix5[1],pix5[2],pix5[3],pix5[4],pix5[5],pix5[6],pix5[7],
            0,0,0,0,0,0,0,0);
            pix5second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix5third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix5fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        }

        else if(lx == 16)
        {
            
            pix1first  = _mm256_set_epi16(pix1[0],pix1[1],pix1[2],pix1[3],pix1[4],pix1[5],pix1[6],pix1[7],
            pix1[8],pix1[9],pix1[10],pix1[11],pix1[12],pix1[13],pix1[14],pix1[15]);
            pix1second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix1third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix1fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

            pix2first  = _mm256_set_epi16(pix2[0],pix2[1],pix2[2],pix2[3],pix2[4],pix2[5],pix2[6],pix2[7],
            pix2[8],pix2[9],pix2[10],pix2[11],pix2[12],pix2[13],pix2[14],pix2[15]);
            pix2second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix2third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix2fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

            pix3first  = _mm256_set_epi16(pix3[0],pix3[1],pix3[2],pix3[3],pix3[4],pix3[5],pix3[6],pix3[7],
            pix3[8],pix3[9],pix3[10],pix3[11],pix3[12],pix3[13],pix3[14],pix3[15]);
            pix3second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix3third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix3fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

            pix4first  = _mm256_set_epi16(pix4[0],pix4[1],pix4[2],pix4[3],pix4[4],pix4[5],pix4[6],pix4[7],
            pix4[8],pix4[9],pix4[10],pix4[11],pix4[12],pix4[13],pix4[14],pix4[15]);
            pix4second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix4third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix4fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);


            pix5first  = _mm256_set_epi16(pix5[0],pix5[1],pix5[2],pix5[3],pix5[4],pix5[5],pix5[6],pix5[7],
            pix5[8],pix5[9],pix5[10],pix5[11],pix5[12],pix5[13],pix5[14],pix5[15]);
            pix5second = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix5third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix5fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        }

        else if(lx == 32)
        {
            pix1first  = _mm256_set_epi16(pix1[0],pix1[1],pix1[2],pix1[3],pix1[4],pix1[5],pix1[6],pix1[7],
            pix1[8],pix1[9],pix1[10],pix1[11],pix1[12],pix1[13],pix1[14],pix1[15]);
            pix1second = _mm256_set_epi16(pix1[16],pix1[17],pix1[18],pix1[19],pix1[20],pix1[21],pix1[22],pix1[23],
            pix1[24],pix1[25],pix1[26],pix1[27],pix1[28],pix1[29],pix1[30],pix1[31]);
            pix1third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix1fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);


            pix2first  = _mm256_set_epi16(pix2[0],pix2[1],pix2[2],pix2[3],pix2[4],pix2[5],pix2[6],pix2[7],
            pix2[8],pix2[9],pix2[10],pix2[11],pix2[12],pix2[13],pix2[14],pix2[15]);
            pix2second = _mm256_set_epi16(pix2[16],pix2[17],pix2[18],pix2[19],pix2[20],pix2[21],pix2[22],pix2[23],
            pix2[24],pix2[25],pix2[26],pix2[27],pix2[28],pix2[29],pix2[30],pix2[31]);
            pix2third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix2fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

            pix3first  = _mm256_set_epi16(pix3[0],pix3[1],pix3[2],pix3[3],pix3[4],pix3[5],pix3[6],pix3[7],
            pix3[8],pix3[9],pix3[10],pix3[11],pix3[12],pix3[13],pix3[14],pix3[15]);
            pix3second = _mm256_set_epi16(pix3[16],pix3[17],pix3[18],pix3[19],pix3[20],pix3[21],pix3[22],pix3[23],
            pix3[24],pix3[25],pix3[26],pix3[27],pix3[28],pix3[29],pix3[30],pix3[31]);
            pix3third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix3fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

            pix4first  = _mm256_set_epi16(pix4[0],pix4[1],pix4[2],pix4[3],pix4[4],pix4[5],pix4[6],pix4[7],
            pix4[8],pix4[9],pix4[10],pix4[11],pix4[12],pix4[13],pix4[14],pix4[15]);
            pix4second = _mm256_set_epi16(pix4[16],pix4[17],pix4[18],pix4[19],pix4[20],pix4[21],pix4[22],pix4[23],
            pix4[24],pix4[25],pix4[26],pix4[27],pix4[28],pix4[29],pix4[30],pix4[31]);
            pix4third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix4fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);


            pix5first  = _mm256_set_epi16(pix5[0],pix5[1],pix5[2],pix5[3],pix5[4],pix5[5],pix5[6],pix5[7],
            pix5[8],pix5[9],pix5[10],pix5[11],pix5[12],pix5[13],pix5[14],pix5[15]);
            pix5second = _mm256_set_epi16(pix5[16],pix5[17],pix5[18],pix5[19],pix5[20],pix5[21],pix5[22],pix5[23],
            pix5[24],pix5[25],pix5[26],pix5[27],pix5[28],pix5[29],pix5[30],pix5[31]);
            pix5third = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
            pix5fourth = _mm256_set_epi16(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
        }

        else if(lx == 64)
        {
            pix1first  = _mm256_set_epi16(pix1[0],pix1[1],pix1[2],pix1[3],pix1[4],pix1[5],pix1[6],pix1[7],
            pix1[8],pix1[9],pix1[10],pix1[11],pix1[12],pix1[13],pix1[14],pix1[15]);
            pix1second = _mm256_set_epi16(pix1[16],pix1[17],pix1[18],pix1[19],pix1[20],pix1[21],pix1[22],pix1[23],
            pix1[24],pix1[25],pix1[26],pix1[27],pix1[28],pix1[29],pix1[30],pix1[31]);
            pix1third = _mm256_set_epi16(pix1[32],pix1[33],pix1[34],pix1[35],pix1[36],pix1[37],pix1[38],pix1[39],
            pix1[40],pix1[41],pix1[42],pix1[43],pix1[44],pix1[45],pix1[46],pix1[47]);
            pix1fourth = _mm256_set_epi16(pix1[48],pix1[49],pix1[50],pix1[51],pix1[52],pix1[53],pix1[54],pix1[55],
            pix1[56],pix1[57],pix1[58],pix1[59],pix1[60],pix1[61],pix1[62],pix1[63]);


            pix2first  = _mm256_set_epi16(pix2[0],pix2[1],pix2[2],pix2[3],pix2[4],pix2[5],pix2[6],pix2[7],
            pix2[8],pix2[9],pix2[10],pix2[11],pix2[12],pix2[13],pix2[14],pix2[15]);
            pix2second = _mm256_set_epi16(pix2[16],pix2[17],pix2[18],pix2[19],pix2[20],pix2[21],pix2[22],pix2[23],
            pix2[24],pix2[25],pix2[26],pix2[27],pix2[28],pix2[29],pix2[30],pix2[31]);
            pix2third = _mm256_set_epi16(pix2[32],pix2[33],pix2[34],pix2[35],pix2[36],pix2[37],pix2[38],pix2[39],
            pix2[40],pix2[41],pix2[42],pix2[43],pix2[44],pix2[45],pix2[46],pix2[47]);
            pix2fourth = _mm256_set_epi16(pix2[48],pix2[49],pix2[50],pix2[51],pix2[52],pix2[53],pix2[54],pix2[55],
            pix2[56],pix2[57],pix2[58],pix2[59],pix2[60],pix2[61],pix2[62],pix2[63]);

            pix3first  = _mm256_set_epi16(pix3[0],pix3[1],pix3[2],pix3[3],pix3[4],pix3[5],pix3[6],pix3[7],
            pix3[8],pix3[9],pix3[10],pix3[11],pix3[12],pix3[13],pix3[14],pix3[15]);
            pix3second = _mm256_set_epi16(pix3[16],pix3[17],pix3[18],pix3[19],pix3[20],pix3[21],pix3[22],pix3[23],
            pix3[24],pix3[25],pix3[26],pix3[27],pix3[28],pix3[29],pix3[30],pix3[31]);
            pix3third = _mm256_set_epi16(pix3[32],pix3[33],pix3[34],pix3[35],pix3[36],pix3[37],pix3[38],pix3[39],
            pix3[40],pix3[41],pix3[42],pix3[43],pix3[44],pix3[45],pix3[46],pix3[47]);
            pix3fourth = _mm256_set_epi16(pix3[48],pix3[49],pix3[50],pix3[51],pix3[52],pix3[53],pix3[54],pix3[55],
            pix3[56],pix3[57],pix3[58],pix3[59],pix3[60],pix3[61],pix3[62],pix3[63]);

            pix4first  = _mm256_set_epi16(pix4[0],pix4[1],pix4[2],pix4[3],pix4[4],pix4[5],pix4[6],pix4[7],
            pix4[8],pix4[9],pix4[10],pix4[11],pix4[12],pix4[13],pix4[14],pix4[15]);
            pix4second = _mm256_set_epi16(pix4[16],pix4[17],pix4[18],pix4[19],pix4[20],pix4[21],pix4[22],pix4[23],
            pix4[24],pix4[25],pix4[26],pix4[27],pix4[28],pix4[29],pix4[30],pix4[31]);
            pix4third = _mm256_set_epi16(pix4[32],pix4[33],pix4[34],pix4[35],pix4[36],pix4[37],pix4[38],pix4[39],
            pix4[40],pix4[41],pix4[42],pix4[43],pix4[44],pix4[45],pix4[46],pix4[47]);
            pix4fourth = _mm256_set_epi16(pix4[48],pix4[49],pix4[50],pix4[51],pix4[52],pix4[53],pix4[54],pix4[55],
            pix4[56],pix4[57],pix4[58],pix4[59],pix4[60],pix4[61],pix4[62],pix4[63]);

            pix5first  = _mm256_set_epi16(pix5[0],pix5[1],pix5[2],pix5[3],pix5[4],pix5[5],pix5[6],pix5[7],
            pix5[8],pix5[9],pix5[10],pix5[11],pix5[12],pix5[13],pix5[14],pix5[15]);
            pix5second = _mm256_set_epi16(pix5[16],pix5[17],pix5[18],pix5[19],pix5[20],pix5[21],pix5[22],pix5[23],
            pix5[24],pix5[25],pix5[26],pix5[27],pix5[28],pix5[29],pix5[30],pix5[31]);
            pix5third = _mm256_set_epi16(pix5[32],pix5[33],pix5[34],pix5[35],pix5[36],pix5[37],pix5[38],pix5[39],
            pix5[40],pix5[41],pix5[42],pix5[43],pix5[44],pix5[45],pix5[46],pix5[47]);
            pix5fourth = _mm256_set_epi16(pix5[48],pix5[49],pix5[50],pix5[51],pix5[52],pix5[53],pix5[54],pix5[55],
            pix5[56],pix5[57],pix5[58],pix5[59],pix5[60],pix5[61],pix5[62],pix5[63]);
        }

        // Take absolute difference of the pix vectors
        res11 = _mm256_or_si256(_mm256_subs_epi8(pix1first,pix2first), _mm256_subs_epi8(pix2first,pix1first));
        res12 = _mm256_or_si256(_mm256_subs_epi8(pix1second,pix2second), _mm256_subs_epi8(pix2second,pix1second)); 
        res13 = _mm256_or_si256(_mm256_subs_epi8(pix1third,pix2third), _mm256_subs_epi8(pix2third,pix1third));
        res14 = _mm256_or_si256(_mm256_subs_epi8(pix1fourth,pix2fourth), _mm256_subs_epi8(pix2fourth,pix1fourth));


        res21 = _mm256_or_si256(_mm256_subs_epi8(pix1first,pix3first), _mm256_subs_epi8(pix3first,pix1first));
        res22 = _mm256_or_si256(_mm256_subs_epi8(pix1second,pix3second), _mm256_subs_epi8(pix3second,pix1second));
        res23 = _mm256_or_si256(_mm256_subs_epi8(pix1third,pix3third), _mm256_subs_epi8(pix3third,pix1third));
        res24 = _mm256_or_si256(_mm256_subs_epi8(pix1fourth,pix3fourth), _mm256_subs_epi8(pix3fourth,pix1fourth)); 

        res31 = _mm256_or_si256(_mm256_subs_epi8(pix1first,pix4first), _mm256_subs_epi8(pix4first,pix1first));
        res32 = _mm256_or_si256(_mm256_subs_epi8(pix1second,pix4second), _mm256_subs_epi8(pix4second,pix1second));
        res33 = _mm256_or_si256(_mm256_subs_epi8(pix1third,pix4third), _mm256_subs_epi8(pix4third,pix1third));
        res34 = _mm256_or_si256(_mm256_subs_epi8(pix1fourth,pix4fourth), _mm256_subs_epi8(pix4fourth,pix1fourth));

        res41 = _mm256_or_si256(_mm256_subs_epi8(pix1first,pix5first), _mm256_subs_epi8(pix5first,pix1first));
        res42 = _mm256_or_si256(_mm256_subs_epi8(pix1second,pix5second), _mm256_subs_epi8(pix5second,pix1second));
        res43 = _mm256_or_si256(_mm256_subs_epi8(pix1third,pix5third), _mm256_subs_epi8(pix5third,pix1third));
        res44 = _mm256_or_si256(_mm256_subs_epi8(pix1fourth,pix5fourth), _mm256_subs_epi8(pix5fourth,pix1fourth));
        
        // Horizontal addition and updating the res[] array
        
        sum = _mm256_hadd_epi16(res11, res12);
        sum_high = _mm256_extractf128_si256(sum, 1);   
        result1 = _mm_add_epi16(sum_high, _mm256_castsi256_si128(sum));
        result1 = _mm_hadd_epi16(result1, result1);
        result1 = _mm_hadd_epi16(result1, result1);
        result1 = _mm_hadd_epi16(result1, result1);

        sum = _mm256_hadd_epi16(res13, res14);
        sum_high = _mm256_extractf128_si256(sum, 1);   
        result2 = _mm_add_epi16(sum_high, _mm256_castsi256_si128(sum));
        result2 = _mm_hadd_epi16(result2, result2);
        result2 = _mm_hadd_epi16(result2, result2);
        result2 = _mm_hadd_epi16(result2, result2);


        res[0] += ((int*)&result1)[0] + ((int*)&result2)[0];

        sum = _mm256_hadd_epi16(res21, res22);
        sum_high = _mm256_extractf128_si256(sum, 1);   
        result1 = _mm_add_epi16(sum_high, _mm256_castsi256_si128(sum));
        result1 = _mm_hadd_epi16(result1, result1);
        result1 = _mm_hadd_epi16(result1, result1);
        result1 = _mm_hadd_epi16(result1, result1);

        sum = _mm256_hadd_epi16(res23, res24);
        sum_high = _mm256_extractf128_si256(sum, 1);   
        result2 = _mm_add_epi16(sum_high, _mm256_castsi256_si128(sum));
        result2 = _mm_hadd_epi16(result2, result2);
        result2 = _mm_hadd_epi16(result2, result2);
        result2 = _mm_hadd_epi16(result2, result2);


        res[1] += ((int*)&result1)[0] + ((int*)&result2)[0];


        sum = _mm256_hadd_epi16(res31, res32);
        sum_high = _mm256_extractf128_si256(sum, 1);   
        result1 = _mm_add_epi16(sum_high, _mm256_castsi256_si128(sum));
        result1 = _mm_hadd_epi16(result1, result1);
        result1 = _mm_hadd_epi16(result1, result1);
        result1 = _mm_hadd_epi16(result1, result1);

        sum = _mm256_hadd_epi16(res33, res34);
        sum_high = _mm256_extractf128_si256(sum, 1);   
        result2 = _mm_add_epi16(sum_high, _mm256_castsi256_si128(sum));
        result2 = _mm_hadd_epi16(result2, result2);
        result2 = _mm_hadd_epi16(result2, result2);
        result2 = _mm_hadd_epi16(result2, result2);


        res[2] += ((int*)&result1)[0] + ((int*)&result2)[0];
        
        sum = _mm256_hadd_epi16(res41, res42);
        sum_high = _mm256_extractf128_si256(sum, 1);   
        result1 = _mm_add_epi16(sum_high, _mm256_castsi256_si128(sum));
        result1 = _mm_hadd_epi16(result1, result1);
        result1 = _mm_hadd_epi16(result1, result1);
        result1 = _mm_hadd_epi16(result1, result1);

        sum = _mm256_hadd_epi16(res43, res44);
        sum_high = _mm256_extractf128_si256(sum, 1);   
        result2 = _mm_add_epi16(sum_high, _mm256_castsi256_si128(sum));
        result2 = _mm_hadd_epi16(result2, result2);
        result2 = _mm_hadd_epi16(result2, result2);
        result2 = _mm_hadd_epi16(result2, result2);


        res[3] += ((int*)&result1)[0] + ((int*)&result2)[0];

        
        pix1 += 64;
        pix2 += frefstride;
        pix3 += frefstride;
        pix4 += frefstride;
        pix5 += frefstride;
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() << "[ns]" << std::endl;
}

int main()
{
		// __m256i first = _mm256_set_epi16(10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25);
  //       __m256i res = _mm256_hadd_epi16(first, first);
  //       __m128i sum_high = _mm256_extracti128_si256(res, 1);  
  //       __m128i result = _mm_add_epi16(sum_high, _mm256_castsi256_si128(res));
  //       result = _mm_hadd_epi16(result, result);	
  //       result = _mm_hadd_epi16(result, result);
  //       printf("%d\n", ((short*)&result)[0]);
	pixel arr[64*64];
	int32_t res[4];
	sad_x4<64,64>(arr, arr, arr, arr, arr, 64, res);

}