#VARIABLES: The registers have the following uses:
# description: this function takes an int array and multiplies
#              every element by 2 and adds 5.
# %edi - Holds the index of the data item being examined
# %ecx - size of the array
# %eax - pointer to first item in array
# %edx - used for scratch space
#
.section .text
.globl asm_mod_array
.type asm_mod_array, @function
asm_mod_array:
pushq %rbp
movq %rsp, %rbp


# movq 8(%rbp),%rax          # get pointer to start of array passed from C
# movq 12(%rbp),%rcx         # get size of array
# xorq %rdi, %rdi            # zero out our array index
 
# start_loop:                # start loop
# cmpq %rdi, %rcx            # check to see if we’ve hit the end
# je loop_exit
# movq (%rax,%rdi,4), %rdx   # store the element in %edx for calculations
# leaq 5(,%rdx,2), %rdx      # multiply array element by 2 and add 5
# # movq %rdx, (%rax,%rdi,4)   # overwrite old element with new value
# addq $1, %rdi                 # increment the index, moving through the array.
# jmp start_loop             # jump to loop beginning
 
loop_exit:                 # function epilogue
movq %rbp, %rsp
popq %rbp
ret                        # pop the return address and jmp to it